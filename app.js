const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) =>{
    res.send('This is my node application CICD demo')    
})

app.listen(port, () => {
    console.log('Application is Listening at http://localhost:${port}')
})